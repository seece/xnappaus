#include <X11/Xlib.h>
#include <stdio.h>
#include <assert.h>
#include <unistd.h>

#define NIL (0) // a void pointer

#define vprint(val) fprintf(stdout, "%s = %d\n", #val, val)

int main(int argc, char* argv[])
{
	Display *dpy = XOpenDisplay(NIL);
	int screen = DefaultScreen(dpy);
	Window root = RootWindow(dpy, screen);
	XImage* img;
	img = XGetImage(dpy, root,1,2,400,300,XAllPlanes(),ZPixmap);

	vprint(screen);
	vprint(img->width);
	vprint(img->height);
	vprint(img->xoffset);
	vprint(img->format);
	vprint(img->byte_order);
	vprint(img->depth);
	vprint(img->bytes_per_line);
	vprint(img->bits_per_pixel);

	return 0;
}
